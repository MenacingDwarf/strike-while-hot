// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class KotlinProject : ModuleRules
{
	public KotlinProject(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { 
			"Core", 
			"CoreUObject", 
			"Engine", 
			"InputCore", 
			"HeadMountedDisplay", 
			"NavigationSystem", 
			"AIModule",
			"Json",
			"JsonUtilities",
			"Http"
		});
    }
}
