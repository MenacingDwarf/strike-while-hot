// Copyright Epic Games, Inc. All Rights Reserved.

#include "Player/KotlinProjectPlayerController.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Player/KotlinProjectCharacter.h"
#include "Engine/World.h"

AKotlinProjectPlayerController::AKotlinProjectPlayerController()
{
}

void AKotlinProjectPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);
}

void AKotlinProjectPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();
}
