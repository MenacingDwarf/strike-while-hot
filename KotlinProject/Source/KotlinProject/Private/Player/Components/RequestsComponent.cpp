// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/Components/RequestsComponent.h"
#include "Interfaces/IHttpResponse.h"
#include "HttpModule.h"
#include "Kismet/KismetSystemLibrary.h"

// Sets default values for this component's properties
URequestsComponent::URequestsComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	Http = &FHttpModule::Get();
	MainURL = "https://ee98b8861102.ngrok.io/";
}


// Called when the game starts
void URequestsComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void URequestsComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void URequestsComponent::SimpleRequestCall(FString URL, FString Method)
{
	TSharedPtr<IHttpRequest, ESPMode::ThreadSafe> Request = Http->CreateRequest();
	Request->OnProcessRequestComplete().BindUObject(this, &URequestsComponent::OnHttpReceive);
	Request->SetURL(MainURL+URL);
	Request->SetVerb(Method);

	Request->SetHeader(TEXT("User-Agent"), TEXT("X-UnrealEngine-Agent"));
	Request->SetHeader(TEXT("Content-Type"), TEXT("Application/json"));

	Request->ProcessRequest();
}

void URequestsComponent::LevelInfoCall(int LevelNumber)
{
	SimpleRequestCall("level_info?level_number=" + FString::FromInt(LevelNumber), "GET");
}

void URequestsComponent::LoginCall(FString Nickname, FString Password)
{
	SimpleRequestCall("login?nickname="+Nickname+"&password="+Password, "POST");
}

void URequestsComponent::RegisterCall(FString Nickname, FString Password)
{
	SimpleRequestCall("register?nickname=" + Nickname + "&password=" + Password, "POST");
}

void URequestsComponent::BestScoreCall(int LevelNumber, int BestScore, FString Method)
{
	SimpleRequestCall("player_level?level_number=" + FString::FromInt(LevelNumber) + "&best_score=" + FString::FromInt(BestScore), Method);
}

void URequestsComponent::GetBestScoresCall()
{
	SimpleRequestCall("player_level", "GET");
}


void URequestsComponent::OnHttpReceive(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessfull)
{
	OnHttpResponse.Broadcast(Response.Get()->GetContentAsString(), bWasSuccessfull);
}

