// Fill out your copyright notice in the Description page of Project Settings.

#include "Player/MenuController.h"
#include "Kismet/GameplayStatics.h"
#include "GameMode/MyGameInstance.h"
#include "Interfaces/WidgetInterface.h"

AMenuController::AMenuController()
{
	RequestsComponent = CreateDefaultSubobject<URequestsComponent>("Requests");
}

bool AMenuController::CheckStatus(FString JsonRaw, bool bWasSuccessfull)
{
	if (!bWasSuccessfull) return false;

	TSharedPtr<FJsonObject> JsonParsed;
	TSharedRef<TJsonReader<TCHAR>> JsonReader = TJsonReaderFactory<TCHAR>::Create(JsonRaw);
	if (!FJsonSerializer::Deserialize(JsonReader, JsonParsed)) return false;

	auto Status = JsonParsed->GetStringField("status");

	return Status == "ok";
}

void AMenuController::OnLoginEnd(FString JsonRaw, bool bWasSuccessfull)
{
	bool IsOk = CheckStatus(JsonRaw, bWasSuccessfull);

	if (IsOk)
	{
		FillLevelsInfo(JsonRaw);
	}


	if (TempObject->Implements<UWidgetInterface>())
	{
		IWidgetInterface::Execute_LoginEnd(TempObject, IsOk);
	}
}

void AMenuController::OnBestScoresEnd(FString JsonRaw, bool bWasSuccessfull)
{
	bool IsOk = CheckStatus(JsonRaw, bWasSuccessfull);

	if (IsOk)
	{
		FillLevelsInfo(JsonRaw);
	}


	if (TempObject->Implements<UWidgetInterface>())
	{
		IWidgetInterface::Execute_BestScoresEnd(TempObject, IsOk);
	}
}

void AMenuController::OnRegisterEnd(FString JsonRaw, bool bWasSuccessfull)
{
	if (TempObject->Implements<UWidgetInterface>())
	{
		IWidgetInterface::Execute_RegisterEnd(TempObject, CheckStatus(JsonRaw, bWasSuccessfull));
	}
}

void AMenuController::FillLevelsInfo(const FString& JsonRaw)
{
	TSharedPtr<FJsonObject> JsonParsed;
	TSharedRef<TJsonReader<TCHAR>> JsonReader = TJsonReaderFactory<TCHAR>::Create(JsonRaw);
	FJsonSerializer::Deserialize(JsonReader, JsonParsed);

	auto LevelsAmount = JsonParsed->GetIntegerField("level_count");
	auto Results = JsonParsed->GetArrayField("results");
	TArray<int> PlayerResults;
	for (auto Result : Results)
	{
		PlayerResults.Add((int)Result->AsNumber());
	}

	if (auto GameInstance = Cast<UMyGameInstance>(UGameplayStatics::GetGameInstance(this)))
	{
		GameInstance->LevelsAmount = LevelsAmount;
		GameInstance->IsAuthorized = true;
		GameInstance->PlayerResults = PlayerResults;
	}
}

void AMenuController::LoginRequest(FString Nickname, FString Password, UObject* Object)
{
	TempObject = Object;
	RequestsComponent->OnHttpResponse.AddDynamic(this, &AMenuController::OnLoginEnd);
	RequestsComponent->LoginCall(Nickname, Password);
}

void AMenuController::RegisterRequest(FString Nickname, FString Password, UObject* Object)
{
	TempObject = Object;
	RequestsComponent->OnHttpResponse.AddDynamic(this, &AMenuController::OnRegisterEnd);
	RequestsComponent->RegisterCall(Nickname, Password);
}

void AMenuController::BestScoresRequest(UObject* Object)
{
	TempObject = Object;
	RequestsComponent->OnHttpResponse.AddDynamic(this, &AMenuController::OnBestScoresEnd);
	RequestsComponent->GetBestScoresCall();
}
