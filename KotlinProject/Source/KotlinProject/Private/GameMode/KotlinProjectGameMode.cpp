// Copyright Epic Games, Inc. All Rights Reserved.

#include "GameMode/KotlinProjectGameMode.h"
#include "Player/KotlinProjectPlayerController.h"
#include "Player/KotlinProjectCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "GameMode/MyGameInstance.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "UObject/ConstructorHelpers.h"

AKotlinProjectGameMode::AKotlinProjectGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AKotlinProjectPlayerController::StaticClass();
	RequestComponent = CreateDefaultSubobject<URequestsComponent>("RequestsComponent");

	LevelDurationSecs = 30;
}

void AKotlinProjectGameMode::SpawnLevelActor(FActorDescription ActorDesc)
{
	auto ActorClass = PossibleActorTypes.Find(ActorDesc.TypeID);
	FVector ActorLocation = { ActorDesc.CoordX, ActorDesc.CoordY, 0.f };
	AActor* NewActor = GetWorld()->SpawnActor<AActor>(ActorClass->Get(), ActorLocation, FRotator(0,0,0));
}

void AKotlinProjectGameMode::BeginPlay()
{
	int CurrentLevel = 1;
	if (auto GameInstance = Cast<UMyGameInstance>(UGameplayStatics::GetGameInstance(this)))
	{
		CurrentLevel = GameInstance->CurrentLevel;
	}
	RequestComponent->OnHttpResponse.AddDynamic(this, &AKotlinProjectGameMode::OnLevelLoaded);
	RequestComponent->LevelInfoCall(CurrentLevel);

	GetWorldTimerManager().SetTimer(LevelTimer, this, &AKotlinProjectGameMode::LevelTimerTick, 1.f, true);
}

void AKotlinProjectGameMode::LevelTimerTick()
{
	CurrentTimeSecs += 1;
	if (CurrentTimeSecs >= LevelDurationSecs)
	{
		EndLevel();
		GetWorldTimerManager().ClearTimer(LevelTimer);
	}
}

void AKotlinProjectGameMode::EndLevel()
{
	if (auto GameInstance = Cast<UMyGameInstance>(UGameplayStatics::GetGameInstance(this)))
	{
		int LastBestScore = GameInstance->PlayerResults[GameInstance->CurrentLevel - 1];
		if (Score != 0 && Score > LastBestScore)
		{
			RequestComponent->BestScoreCall(GameInstance->CurrentLevel, Score, LastBestScore > 0 ? "PUT" : "POST");
		}
	}

	if (auto PlayerController = Cast< AKotlinProjectPlayerController>(GetWorld()->GetFirstPlayerController()))
	{
		PlayerController->OnLevelEnd();
	}
}

void AKotlinProjectGameMode::OnLevelLoaded(FString JsonRaw, bool bWasSuccessfull)
{
	if (!bWasSuccessfull) return;

	TSharedPtr<FJsonObject> JsonParsed;
	TSharedRef<TJsonReader<TCHAR>> JsonReader = TJsonReaderFactory<TCHAR>::Create(JsonRaw);
	if (!FJsonSerializer::Deserialize(JsonReader, JsonParsed)) return;

	auto Actors = JsonParsed->GetArrayField("actors");
	auto ActorTypes = JsonParsed->GetArrayField("actor_types");

	for (auto ActorTypeObj : ActorTypes)
	{
		auto ActorType = FActorType(ActorTypeObj->AsObject());
		PossibleActorTypes.Add(ActorType.id, ActorType.ActorClass);
	}

	for (auto ActorObj : Actors)
	{
		auto ActorDesc = FActorDescription(ActorObj->AsObject());
		SpawnLevelActor(ActorDesc);
	}
}

