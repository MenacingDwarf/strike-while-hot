// Copyright Epic Games, Inc. All Rights Reserved.

#include "KotlinProject.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, KotlinProject, "KotlinProject" );

DEFINE_LOG_CATEGORY(LogKotlinProject)
 