// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "WidgetInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(BlueprintType)
class UWidgetInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class KOTLINPROJECT_API IWidgetInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Authorization")
	void LoginEnd(bool bWasSuccessful);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Authorization")
	void RegisterEnd(bool bWasSuccessful);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "LevelsInfo")
	void BestScoresEnd(bool bWasSuccessful);
};
