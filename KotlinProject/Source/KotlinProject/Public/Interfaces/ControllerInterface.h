// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "ControllerInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(BlueprintType)
class UControllerInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class KOTLINPROJECT_API IControllerInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Authorization")
	void Login(const FString& Nickname, const FString& Password, UObject* Object);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Authorization")
	void Register(const FString& Nickname, const FString& Password, UObject* Object);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "LevelsInfo")
	void BestScores(UObject* Object);
};
