// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "KotlinProjectPlayerController.generated.h"

UCLASS()
class AKotlinProjectPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	AKotlinProjectPlayerController();

	UFUNCTION(BlueprintImplementableEvent)
	void OnLevelEnd();

protected:

	// Begin PlayerController interface
	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	// End PlayerController interface
};


