// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "KotlinProjectCharacter.generated.h"

UCLASS(Blueprintable)
class AKotlinProjectCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	AKotlinProjectCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;
};

