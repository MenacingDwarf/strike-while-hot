// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Interfaces/IHttpRequest.h"
#include "HttpModule.h"
#include "Json.h"
#include "RequestsComponent.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FHttpResponseDelegate, FString, JsonResponse, bool, ResponseSuccessful);


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class KOTLINPROJECT_API URequestsComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	URequestsComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	FHttpModule* Http;

	UFUNCTION()
	void SimpleRequestCall(FString URL, FString Method);

	void OnHttpReceive(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessfull);

	UPROPERTY(EditDefaultsOnly)
	FString MainURL;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	FString JsonResponse;
	bool ResponseSuccessful;

	UPROPERTY(BlueprintAssignable)
	FHttpResponseDelegate OnHttpResponse;

	// Different requests

	UFUNCTION(BlueprintCallable)
	void LevelInfoCall(int LevelNumber = 1);

	UFUNCTION(BlueprintCallable)
	void LoginCall(FString Nickname, FString Password);

	UFUNCTION(BlueprintCallable)
	void RegisterCall(FString Nickname, FString Password);

	UFUNCTION(BlueprintCallable)
	void BestScoreCall(int LevelNumber, int BestScore, FString Method);

	UFUNCTION(BlueprintCallable)
	void GetBestScoresCall();
};
