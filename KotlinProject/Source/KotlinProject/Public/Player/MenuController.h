// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Player/Components/RequestsComponent.h"
#include "MenuController.generated.h"

/**
 * 
 */


UCLASS()
class KOTLINPROJECT_API AMenuController : public APlayerController
{
	GENERATED_BODY()

public:
	AMenuController();

private:

	URequestsComponent* RequestsComponent;

	UFUNCTION()
	void OnLoginEnd(FString JsonRaw, bool bWasSuccessfull);

	UFUNCTION()
	void OnBestScoresEnd(FString JsonRaw, bool bWasSuccessfull);

	UFUNCTION()
	void OnRegisterEnd(FString JsonRaw, bool bWasSuccessfull);

	bool CheckStatus(FString JsonRaw, bool bWasSuccessfull);

	UObject* TempObject;

	void FillLevelsInfo(const FString& JsonRaw);

public:
	UFUNCTION(BlueprintCallable)
	void LoginRequest(FString Nickname, FString Password, UObject* Object);

	UFUNCTION(BlueprintCallable)
	void RegisterRequest(FString Nickname, FString Password, UObject* Object);

	UFUNCTION(BlueprintCallable)
	void BestScoresRequest(UObject* Object);
};
