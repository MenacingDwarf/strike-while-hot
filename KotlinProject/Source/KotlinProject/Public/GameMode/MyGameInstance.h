// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "MyGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class KOTLINPROJECT_API UMyGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:

	UMyGameInstance();

	UPROPERTY(BlueprintReadWrite)
	int LevelsAmount;

	UPROPERTY(BlueprintReadWrite)
	bool IsAuthorized;

	UPROPERTY(BlueprintReadWrite)
	int CurrentLevel;

	UPROPERTY(BlueprintReadWrite)
	TArray<int> PlayerResults;
	
};
