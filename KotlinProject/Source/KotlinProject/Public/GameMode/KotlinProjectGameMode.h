// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Player/Components/RequestsComponent.h"
#include "Json.h"
#include "KotlinProjectGameMode.generated.h"

USTRUCT(BlueprintType)
struct FActorType
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite)
	int id;

	UPROPERTY(BlueprintReadWrite)
	TSubclassOf<AActor> ActorClass;

	FActorType()
	{
		id = 0;
		ActorClass = AActor::StaticClass();
	}

	FActorType(TSharedPtr<FJsonObject> Json)
	{
		id = Json->GetIntegerField("id");
		ActorClass = StaticLoadClass(AActor::StaticClass(), NULL, *Json->GetStringField("actorPath"));
	}
};

USTRUCT(BlueprintType)
struct FActorDescription
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite)
	int TypeID;
	
	UPROPERTY(BlueprintReadWrite)
	float CoordX;

	UPROPERTY(BlueprintReadWrite)
	float CoordY;

	FActorDescription()
	{
		TypeID = 0;
		CoordX = 0.f;
		CoordY = 0.f;
	}

	FActorDescription(TSharedPtr<FJsonObject> Json)
	{
		TypeID = Json->GetIntegerField("actorType");
		CoordX = Json->GetNumberField("x");
		CoordY = Json->GetNumberField("y");
	}
};


UCLASS(minimalapi)
class AKotlinProjectGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AKotlinProjectGameMode();

	URequestsComponent* RequestComponent;

	UFUNCTION()
	void OnLevelLoaded(FString JsonRaw, bool bWasSuccessfull);

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int LevelDurationSecs;

	UPROPERTY(BlueprintReadWrite)
	int CurrentTimeSecs;

	UPROPERTY(BlueprintReadWrite)
	int Score;

protected:

	TMap<int, TSubclassOf<AActor> > PossibleActorTypes;

	void SpawnLevelActor(FActorDescription ActorDesc);

	virtual void BeginPlay() override;

	FTimerHandle LevelTimer;

	UFUNCTION()
	void LevelTimerTick();

	UFUNCTION()
	void EndLevel();

};



