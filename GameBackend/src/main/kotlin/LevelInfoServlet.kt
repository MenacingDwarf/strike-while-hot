import utils.DBHelper
import com.google.gson.Gson
import utils.MainClasses
import utils.UserRole
import utils.checkRole
import java.io.BufferedReader
import java.io.IOException
import javax.servlet.annotation.*
import javax.servlet.http.*
import utils.MainClasses.LevelInfo as LevelInfo

@WebServlet(name = "LevelInfoServlet", urlPatterns = ["/level_info"])
class LevelInfoServlet : HttpServlet() {
    private val gson: Gson = Gson()
    private val db = DBHelper()

    @Throws(IOException::class)
    override fun doGet(
        request: HttpServletRequest,
        response: HttpServletResponse
    ) {
        val levelNumber = request.getParameter("level_number").toInt()

        val actors = db.allActors(levelNumber).map{ MainClasses.Actor(it.actorType, it.x * 2000, it.y * 1000) };
        val actorTypes = db.allActorTypes()

        val employeeJsonString: String = gson.toJson(mapOf("actors" to actors, "actor_types" to actorTypes))
        val out = response.writer
        response.contentType = "application/json"
        response.characterEncoding = "UTF-8"
        out.print(employeeJsonString)
        out.flush()
    }

    override fun doPut(
        request: HttpServletRequest,
        response: HttpServletResponse
    ) {
        val out = response.writer
        response.contentType = "application/json"
        response.characterEncoding = "UTF-8"

        if (!checkRole(request, UserRole.Admin)) {
            out.print("{\"status\": \"not ok\"}")
            out.flush()
            return
        }

        var jb: String  = ""
        var line: String? = null
        val reader: BufferedReader = request.reader
        line = reader.readLine()
        while (line != null) {
            jb += line
            line = reader.readLine()
        }
        println(jb)
        val levelInfo = gson.fromJson(jb, LevelInfo::class.java)
        db.rewriteLevel(levelInfo)

        out.print("{\"status\": \"ok\"}")
        out.flush()
    }

    override fun doPost(
        request: HttpServletRequest,
        response: HttpServletResponse
    ) {
        if (!checkRole(request, UserRole.Admin)) {
            response.sendRedirect(request.contextPath + "/login")
            return
        }

        db.addLevel()
        val levelNumber = db.countLevels()
        response.sendRedirect(request.contextPath + "/main?level_number=${levelNumber}")
    }
}