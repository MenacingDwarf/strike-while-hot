package utils

import java.sql.Connection
import java.sql.DriverManager
import java.util.*
import utils.MainClasses.Actor as Actor
import utils.MainClasses.ActorType as ActorType
import utils.MainClasses.LevelInfo as LevelInfo
import utils.MainClasses.PlayerResult as PlayerResult
import java.sql.ResultSet
import java.sql.Statement

class DBHelper {
    var connection: Connection? = null

    fun init() {
    }

    private fun initConnection() {
        Class.forName("org.postgresql.Driver");
        val url = "jdbc:postgresql://localhost:5432/game_backend"
        val props = Properties()
        props.setProperty("user", "postgres")
        props.setProperty("password", "password")
        this.connection = DriverManager.getConnection(url, props)
    }

    private fun simpleExecute(query: String) {
        initConnection()

        var statement: Statement? = null
        try {
            statement = connection?.createStatement()
            statement?.execute(query)
        }
        catch (e: Exception){
            e.printStackTrace()
        }
        finally {
            statement?.close()
            connection?.close()
        }
    }

    private fun simpleSelect(query: String, selectedField: String): String {
        initConnection()
        var statement: Statement? = null
        var field: String = ""
        try {
            statement = this.connection?.createStatement()
            val result: ResultSet? = statement?.executeQuery(query)

            while (result?.next() == true) {
                field = result.getString(selectedField)
            }

        }
        catch (e: Exception){
            e.printStackTrace()
        }
        finally {
            statement?.close()
            connection?.close()
        }

        return field
    }

    fun allActors(level_number: Int): MutableList<Actor> {
        initConnection()
        val sqlQuery = "select * from actor, actor_type, \"level\" " +
                       "where \"level\".number = $level_number and actor.level_id = \"level\".id " +
                       "and actor.actor_type_id = actor_type.id"
        var statement: Statement? = null
        val actors: MutableList<Actor> = mutableListOf()
        try {
            statement = this.connection?.createStatement()
            val result: ResultSet? = statement?.executeQuery(sqlQuery)

            while (result?.next() == true) {
                val newActor: Actor = Actor(result.getInt("actor_type_id"),
                                            result.getFloat("x_coord"),
                                            result.getFloat("y_coord"))
                actors.add(newActor)
            }

        }
        catch (e: Exception){
            e.printStackTrace()
        }
        finally {
            statement?.close()
            connection?.close()
        }

        return actors
    }

    fun allActorTypes(): MutableList<ActorType> {
        initConnection()
        val sqlQuery = "select * from actor_type"
        var statement: Statement? = null
        val actorTypes: MutableList<ActorType> = mutableListOf()
        try {
            statement = this.connection?.createStatement()
            val result: ResultSet? = statement?.executeQuery(sqlQuery)

            while (result?.next() == true) {
                val newActorType: ActorType = ActorType(result.getInt("id"),
                                                        result.getString("name"),
                                                        result.getString("actor_path"),
                                                        result.getInt("size_x"),
                                                        result.getInt("size_y"),
                                                        result.getString("ui_color"))
                actorTypes.add(newActorType)
            }

        }
        catch (e: Exception){
            e.printStackTrace()
        }
        finally {
            statement?.close()
            connection?.close()
        }

        return actorTypes
    }

    fun registerUser(username: String, password: String): Boolean {
        val isExist = simpleSelect("select count(id) as is_exist from \"user\" where nickname='$username';", "is_exist").toInt() > 0
        if (isExist) return false

        simpleExecute("INSERT INTO \"user\" (nickname, password) VALUES ('$username', '$password');")
        val newId = simpleSelect("select id from \"user\" where nickname='$username';", "id").toInt()
        simpleExecute("insert into player values(default, $newId);")
        return true
    }

    fun rewriteLevel(levelInfo: LevelInfo) {
        simpleExecute("delete from actor where level_id=${levelInfo.levelNumber};")
        for (actor in levelInfo.actors) {
            simpleExecute("insert into actor values(default, ${levelInfo.levelNumber}, ${actor.actorType}, ${actor.x}, ${actor.y});")
        }
    }

    fun getUserPassword(username: String): String {
        return simpleSelect("select password from \"user\" where nickname='$username'", "password")
    }

    fun getUserRole(username:String): UserRole {
        return when (simpleSelect("select count(*) as is_admin from \"user\",\"admin\" where nickname='$username' and \"admin\".user_id = \"user\".id;", "is_admin").toInt()) {
            0->UserRole.Player
            else->UserRole.Admin
        }
    }

    fun countLevels(): Int {
        return simpleSelect("select max(number) as levels_amount from \"level\"", "levels_amount").toInt()
    }

    fun addLevel() {
        val newLevelNumber = countLevels() + 1
        simpleExecute("insert into level values(default, ${newLevelNumber});")
    }

    fun getResults(username: String) : MutableList<PlayerResult>{
        initConnection()
        val sqlQuery = "select * from player_level, player, \"user\", \"level\" where \"user\".nickname='$username' and \"user\".id = player.user_id and player.id = player_level.player_id and \"level\".id = player_level.level_id"
        var statement: Statement? = null
        val playerResults: MutableList<PlayerResult> = mutableListOf()
        try {
            statement = this.connection?.createStatement()
            val result: ResultSet? = statement?.executeQuery(sqlQuery)

            while (result?.next() == true) {
                val newPlayerResult: PlayerResult = PlayerResult(
                    result.getInt("number"),
                    result.getInt("best_score")
                )
                playerResults.add(newPlayerResult)
            }

        }
        catch (e: Exception){
            e.printStackTrace()
        }
        finally {
            statement?.close()
            connection?.close()
        }

        return playerResults
    }

    fun getBestScore(username: String, levelNumber: Int) : Number? {
        val scoreString = simpleSelect("select best_score from \"user\", player, player_level, \"level\" where \"user\".nickname='$username' and \"user\".id=player.user_id and \"level\".number=$levelNumber and player_level.player_id=player.id and player_level.level_id=\"level\".id ", "best_score")
        if (scoreString.isEmpty()) return null
        return scoreString.toInt()
    }

    fun insertBestScore(username: String, levelNumber: Int, newBestScore: Int) {
        val playerId = simpleSelect("select player.id as player_id from player, \"user\" where \"user\".id = user_id and nickname='$username'", "player_id").toInt()
        val levelId = simpleSelect("select id from \"level\" where number=$levelNumber", "id").toInt()
        simpleExecute("insert into player_level values($playerId, $levelId, $newBestScore)")
    }
    fun updateBestScore(username: String, levelNumber: Int, newBestScore: Int) {
        val playerId = simpleSelect("select player.id as player_id from player, \"user\" where \"user\".id = user_id and nickname='$username'", "player_id").toInt()
        val levelId = simpleSelect("select id from \"level\" where number=$levelNumber", "id").toInt()
        simpleExecute("update player_level set best_score=$newBestScore where player_id=$playerId and level_id=$levelId")
    }
}