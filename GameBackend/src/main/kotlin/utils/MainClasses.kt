package utils

import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpSession

enum class UserRole {
    Player, Admin, Creator
}

fun checkRole(request: HttpServletRequest, requiredRole: UserRole): Boolean {
    val session: HttpSession = request.session
    if (session.getAttribute("is_authenticated") != true) return false
    return session.getAttribute("user_role") == requiredRole
}

fun isUE4(request: HttpServletRequest): Boolean {
    val headerNames: Enumeration<*> = request.headerNames
    while (headerNames.hasMoreElements()) {
        val key = headerNames.nextElement() as String
        if (key == "user-agent") {
            val value = request.getHeader(key)
            return value == "X-UnrealEngine-Agent"
        }
    }

    return false
}

class MainClasses {

    class Actor(actorType_: Int, x_: Float, y_: Float) {
        var actorType: Int = actorType_
        var x: Float = x_
        var y: Float = y_

        fun getType(types: Array<ActorType>): ActorType? {
            for (type in types) {
                if (type.id == actorType) return type
            }
            return null
        }
    }

    class ActorType(id_: Int, name_: String, actorPath_: String, sizeX_: Int, sizeY_: Int, UIColor_: String) {
        var id: Int = id_
        var name: String = name_
        var actorPath: String = actorPath_
        var UIColor: String = UIColor_
        var sizeX: Int = sizeX_
        var sizeY: Int = sizeY_
    }

    class LevelInfo(levelNumber_: Int, actors_: Array<Actor>) {
        var levelNumber: Int = levelNumber_
        var actors: Array<Actor> = actors_
    }

    class PlayerResult(levelId_: Int, bestScore_: Int) {
        var levelId: Int = levelId_
        var bestScore: Int = bestScore_
    }


}