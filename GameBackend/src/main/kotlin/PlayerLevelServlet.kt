import org.mindrot.jbcrypt.BCrypt
import utils.DBHelper
import com.google.gson.Gson
import utils.UserRole
import utils.checkRole
import utils.isUE4
import java.io.IOException
import java.util.*
import javax.servlet.annotation.WebServlet
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.servlet.http.HttpSession


@WebServlet(name = "PlayerLevelServlet", urlPatterns = ["/player_level"])
class PlayerLevelServlet : HttpServlet() {

    private val gson: Gson = Gson()
    private val db: DBHelper = DBHelper()

    override fun doGet(
        request: HttpServletRequest,
        response: HttpServletResponse
    ) {
        val session: HttpSession = request.session
        val username = session.getAttribute("username").toString()

        val levelCount = db.countLevels()
        val playerResults = db.getResults(username)

        var finalPlayerResult: MutableList<Int> = MutableList(levelCount) { 0 }
        for (result in playerResults) {
            finalPlayerResult[result.levelId-1] = result.bestScore
        }

        val out = response.writer
        response.contentType = "application/json"
        response.characterEncoding = "UTF-8"
        out.print("{\"status\": \"ok\", \"level_count\": ${db.countLevels()}, \"results\": ${gson.toJson(finalPlayerResult)}}")
        out.flush()
    }

    override fun doPost(
        request: HttpServletRequest,
        response: HttpServletResponse
    ) {
        val session: HttpSession = request.session
        val username = session.getAttribute("username").toString()
        val levelNumber = request.getParameter("level_number").toInt()
        val bestScore = request.getParameter("best_score").toInt()

        var status = "ok"

        if (!isUE4(request) || !checkRole(request, UserRole.Player)) status = "not ok"
        else if (db.getBestScore(username, levelNumber) != null) status = "not ok"
        else db.insertBestScore(username, levelNumber, bestScore)

        val out = response.writer
        response.contentType = "application/json"
        response.characterEncoding = "UTF-8"
        out.print("{\"status\": \"$status\"}")
        out.flush()

    }

    override fun doPut(
        request: HttpServletRequest,
        response: HttpServletResponse
    ) {
        val session: HttpSession = request.session
        val username = session.getAttribute("username").toString()
        val levelNumber = request.getParameter("level_number").toInt()
        val bestScore = request.getParameter("best_score").toInt()

        var status = "ok"

        if (!isUE4(request) || !checkRole(request, UserRole.Player)) status = "not ok"
        else if (db.getBestScore(username, levelNumber) == null) status = "not ok"
        else db.updateBestScore(username, levelNumber, bestScore)

        val out = response.writer
        response.contentType = "application/json"
        response.characterEncoding = "UTF-8"
        out.print("{\"status\": \"$status\"}")
        out.flush()
    }
}