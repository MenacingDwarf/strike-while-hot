import utils.DBHelper
import org.mindrot.jbcrypt.BCrypt
import com.google.gson.Gson
import java.io.IOException
import javax.servlet.annotation.*
import javax.servlet.http.*

@WebServlet(name = "RegisterServlet", urlPatterns = ["/register"])
class RegisterServlet : HttpServlet() {
    @Throws(IOException::class)
    override fun doPost(
        request: HttpServletRequest,
        response: HttpServletResponse
    ) {
        val username = request.getParameter("nickname")
        val password = request.getParameter("password")

        val out = response.writer
        response.contentType = "application/json"
        response.characterEncoding = "UTF-8"

        val db = DBHelper()
        if (!db.registerUser(username, BCrypt.hashpw(password, BCrypt.gensalt()))) {
            out.print("{\"status\": \"not ok\"}")
            out.flush()
            return
        }

        out.print("{\"status\": \"ok\"}")
        out.flush()
    }
}