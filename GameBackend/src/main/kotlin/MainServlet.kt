import utils.DBHelper
import com.google.gson.Gson
import utils.UserRole
import utils.checkRole
import javax.servlet.annotation.*
import javax.servlet.http.*

@WebServlet(name = "MainServlet", urlPatterns = ["/main"])
class MainServlet : HttpServlet() {
    private val db = DBHelper()
    private val gson: Gson = Gson()

    override fun doGet(
        request: HttpServletRequest,
        response: HttpServletResponse
    ) {
        response.contentType = "text/html"

        if (!checkRole(request, UserRole.Admin)) {
            response.sendRedirect(request.contextPath + "/login")
            return
        }

        request.setAttribute("levels_amount", db.countLevels())

        // check if should get certain level
        val levelNumber = request.getParameter("level_number")
        if (levelNumber == null) {
            request.getRequestDispatcher("/main.jsp").forward(request, response)
            return
        }

        // add attributes and load jsp
        val actors = db.allActors(levelNumber.toInt())
        val actorTypes = db.allActorTypes()
        val actorsString: String = gson.toJson(actors)
        val actorTypesString: String = gson.toJson(actorTypes)
        request.setAttribute("level_actors", actorsString)
        request.setAttribute("actor_types", actorTypesString)
        request.getRequestDispatcher("/main.jsp").forward(request, response)
    }
}