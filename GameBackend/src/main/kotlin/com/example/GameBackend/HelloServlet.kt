package com.example.GameBackend

import javax.servlet.annotation.*
import javax.servlet.http.*


@WebServlet(name = "helloServlet", value = ["/hello-servlet"])
class HelloServlet : HttpServlet() {

    public override fun doGet(request: HttpServletRequest, response: HttpServletResponse) {
        response.sendRedirect(request.contextPath + "/main")
    }

    override fun destroy() {
    }
}