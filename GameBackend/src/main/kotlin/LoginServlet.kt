import org.mindrot.jbcrypt.BCrypt
import utils.DBHelper
import com.google.gson.Gson
import utils.isUE4
import java.io.IOException
import java.util.*
import javax.servlet.annotation.WebServlet
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.servlet.http.HttpSession


@WebServlet(name = "LoginServlet", urlPatterns = ["/login"])
class LoginServlet : HttpServlet() {

    private val gson: Gson = Gson()

    @Throws(IOException::class)
    override fun doPost(
        request: HttpServletRequest,
        response: HttpServletResponse
    ) {
        val username = request.getParameter("nickname")
        val password = request.getParameter("password")

        val db = DBHelper()
        val dbPassword = db.getUserPassword(username)
        val session: HttpSession = request.session

        if (BCrypt.checkpw(password, dbPassword)) {
            session.setAttribute("is_authenticated", true)
            session.setAttribute("user_role", db.getUserRole(username))

            if (isUE4(request)) {
                session.setAttribute("username", username)
                val levelCount = db.countLevels()
                val playerResults = db.getResults(username)

                var finalPlayerResult: MutableList<Int> = MutableList(levelCount) { 0 }
                for (result in playerResults) {
                    finalPlayerResult[result.levelId-1] = result.bestScore
                }

                val out = response.writer
                response.contentType = "application/json"
                response.characterEncoding = "UTF-8"
                out.print("{\"status\": \"ok\", \"level_count\": ${db.countLevels()}, \"results\": ${gson.toJson(finalPlayerResult)}}")
                out.flush()
            }
            else {
                response.sendRedirect(request.contextPath + "/main")
            }

        }
        else {
            session.setAttribute("is_authenticated", false)

            if (isUE4(request)) {
                val out = response.writer
                response.contentType = "application/json"
                response.characterEncoding = "UTF-8"
                out.print("{\"status\": \"not ok\"}")
                out.flush()
            }
            else {
                response.sendRedirect(request.contextPath + "/login")
            }
        }
    }

    override fun doGet(
        request: HttpServletRequest,
        response: HttpServletResponse
    ) {
        response.contentType = "text/html"

        request.getRequestDispatcher("/login.jsp").forward(request, response)
    }
}