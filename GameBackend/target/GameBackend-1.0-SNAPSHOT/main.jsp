<%@ page import="javax.swing.plaf.synth.SynthDesktopIconUI" %>
<%@ page import="utils.MainClasses" %>
<%@ page import="com.google.gson.Gson" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<%
    Gson gson = new Gson();

    int levels_amount = (int)request.getAttribute("levels_amount");
    String actors_string = (String) request.getAttribute("level_actors");
    MainClasses.Actor[] actors = gson.fromJson(actors_string, MainClasses.Actor[].class);
    String actor_types_string = (String) request.getAttribute("actor_types");
    MainClasses.ActorType[] actor_types = gson.fromJson(actor_types_string, MainClasses.ActorType[].class);

    int border_width = 4;
%>

<html>
<head>
    <title>Main page</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <style>
        .level_zone {
            width: 100%;
            border: <%=border_width%>px solid black;
            position: relative;
        }

        .level_zone:after {
            content: "";
            display: block;
            padding-bottom: 50%;
        }
    </style>
</head>
<body>
<div class="container">
    <h1>Level constructor</h1>
    <div class="mt-3 row">


        <%-- Levels list --%>
        <div class="col col-2">
            <h3>Select level</h3>
            <div class="list-group">
                <%
                    int activeLevel = request.getParameter("level_number") == null ? -1 : Integer.parseInt(request.getParameter("level_number"));
                    for (int i = 1; i<=levels_amount; i++) {
                        String activeTag = i == activeLevel ? "active\" aria-current=\"true" : "";
                %>
                <a href="${pageContext.request.contextPath}/main?level_number=<%=i%>" class="list-group-item list-group-item-action <%=activeTag%>">
                    Level <%=i%>
                </a>
                <%}%>
            </div>
            <center>
                <form method="post" action="${pageContext.request.contextPath}/level_info">
                    <input type="submit" value="New level" class="btn btn-success mt-2" />
                </form>
            </center>
        </div>


        <%-- Actor types list --%>
        <div class="col col-3">
            <h3>Select actor type</h3>
            <% if (activeLevel != -1) for (MainClasses.ActorType actorType : actor_types) { %>
            <a href="#" class="list-group-item list-group-item-action actor-type" data-id="<%=actorType.getId()%>" id="actor-type-<%=actorType.getId()%>">
                <%=actorType.getName()%>
            </a>
            <%}%>
        </div>


        <%-- Actor types list --%>
        <div class="col col-7">
            <h3>Place selected actor type</h3>
            <% if (activeLevel != -1) { %>
            <div class="level_zone" id="level_zone">
                <% for (MainClasses.Actor actor : actors) { %>
                    <div class="actor" data-x="<%=actor.getX()%>" data-y="<%=actor.getY()%>" data-type="<%=actor.getType(actor_types).getId()%>" style="position: absolute; top: <%=actor.getY()%>px; left: <%=actor.getX()%>px; background-color: <%=actor.getType(actor_types).getUIColor()%>; width:<%=actor.getType(actor_types).getSizeX()%>; height:<%=actor.getType(actor_types).getSizeY()%>"></div>
                <%}%>
            </div>
            <center>
                <button class="btn btn-primary mt-2" type="button" id="save_button">Save</button>
            </center>
            <p>Use <b>Left Mouse Button</b> to place</p>
            <p>Use <b>Right Mouse Button</b> to delete</p>
            <%}%>
        </div>


    </div>
</div>

<script>
    let current_actor_type_id = -1;
    let actor_types_array = <%=actor_types_string%>;
    let border_width = <%=border_width%>;
    let current_level = <%=request.getParameter("level_number")%>;

    let level_zone = document.getElementById("level_zone");
    let zone_width = level_zone.clientWidth;
    let zone_height = level_zone.clientHeight;

    let add_rmb_action = function(element) {
        element.addEventListener('contextmenu', function(ev) {
            ev.preventDefault();
            let target = ev.target;
            target.parentNode.removeChild(target);
            return false;
        }, false);
    }

    for (let actor of document.getElementsByClassName("actor")) {
        actor.style.left = zone_width * actor.dataset.x + "px"
        actor.style.top = zone_height * actor.dataset.y + "px"
        add_rmb_action(actor);
    }


    level_zone.addEventListener('click', function (e) {
        const target = e.target;

        // Get the bounding rectangle of target
        const rect = target.getBoundingClientRect();

        // Mouse position
        const x = e.clientX - rect.left - border_width;
        const y = e.clientY - rect.top - border_width;


        let actor_type_info = actor_types_array.find(item => item.id === current_actor_type_id);
        if (actor_type_info) {
            let new_actor = document.createElement("div");
            new_actor.style.position = "absolute";
            new_actor.style.top = y + "px";
            new_actor.style.left = x + "px";
            new_actor.style.width = actor_type_info.sizeX + "px";
            new_actor.style.height = actor_type_info.sizeY + "px";
            new_actor.style.backgroundColor = actor_type_info.UIColor;

            new_actor.classList.add("actor");
            new_actor.setAttribute("data-type", current_actor_type_id.toString());
            new_actor.setAttribute("data-x", (x/zone_width).toString());
            new_actor.setAttribute("data-y", (y/zone_height).toString());

            add_rmb_action(new_actor);
            document.getElementById("level_zone").appendChild(new_actor);
        }
    })

    let change_actor_type_active = function(actor_type_id, is_active) {
        let current_actor_type = document.getElementById("actor-type-"+actor_type_id);
        if (current_actor_type !== null) {
            if (is_active) {
                current_actor_type.setAttribute("aria-current", "true");
                current_actor_type.classList.add("active");
            }
            else {
                current_actor_type.removeAttribute("aria-current");
                current_actor_type.classList.remove("active");
            }
        }
    }

    for (let actor_type of document.getElementsByClassName("actor-type")) {
        actor_type.addEventListener('mousedown', function (e) {
            change_actor_type_active(current_actor_type_id, false);
            const target = e.target;
            current_actor_type_id = Number(target.dataset.id);
            change_actor_type_active(current_actor_type_id, true);
        })
    }

    let save_button = document.getElementById("save_button")
    save_button.addEventListener("click", function() {
        let current_actors = Array.from(document.getElementsByClassName("actor")).map(
                                item => ({
                                    actorType: Number(item.dataset.type),
                                    x: Number(item.dataset.x),
                                    y: Number(item.dataset.y)
                                }));
        console.log(current_actors);
        let xhr = new XMLHttpRequest();

        let json = JSON.stringify({
            levelNumber: current_level,
            actors: current_actors
        });

        xhr.open("PUT", "${pageContext.request.contextPath}/level_info")
        xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');

        xhr.send(json);
        alert("Level was saved!");
    })
</script>
</body>
</html>
