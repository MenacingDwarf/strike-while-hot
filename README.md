# Strike While Hot

Небольшой проект, реализующий связь Unreal Engine приложения с сервером, написанном на Kotlin. Проект представляет собой простую игру по типу Overcooked только в разрезе кузнечного дела.

В папке KotlinProject находится клиентская часть приложения (проект на Unreal Engine 4.26). "Фишка" проекта в том, чтобы составлять уровни в игре с помощью веб-интерфейса. При этом в Unreal Engine уровни подгружаются с сервера посредством http запросов.

Сайт для администраторов: https://ee98b8861102.ngrok.io/main

Архив с игрой: https://drive.google.com/file/d/1DArkrMj4JdLhBgYncBN1yhNXzFvMcSGI/view?usp=sharing
